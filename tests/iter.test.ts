import { assertEquals } from "../deps.ts";
import { assignIterFunc } from "../iter_functions.ts";

function* numbers(max: number) {
  for (let i = 0; i < max; i++) {
    yield i;
  }
}

function reuseAble(gen: any, ...args: any[]) {
  return {
    [Symbol.iterator]: function* () {
      yield* gen(...args);
    },
  };
}

Deno.test("iterFunctions", async (): Promise<void> => {
  const iter = assignIterFunc(reuseAble(numbers, 2));
  assertEquals([0, 1], iter.collect(true));

  let extra = 10;
  const mapped = iter.map((value) => value + extra);
  assertEquals([10, 11], mapped.collect(true));
  extra = 20;
  assertEquals([20, 21], mapped.collect(true));

  const filtered = iter.filter((value) => value === 1);
  assertEquals([1], filtered.collect(true));

  const filter_mapped = filtered.map((value) => value + 10);
  assertEquals([11], filter_mapped.collect(true));
});
