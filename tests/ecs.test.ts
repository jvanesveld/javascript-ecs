import { assertEquals } from "../deps.ts";
import { ECS } from "../mod.ts";

function createBaseECS() {
  const ecs = new ECS();
  ecs.addEntity({});
  ecs.addEntity({});
  ecs.addEntity({});
  return ecs;
}

Deno.test("addEntity", async (): Promise<void> => {
  const ecs = new ECS();
  const ids = ecs.getComponentIterator(["id"]);
  assertEquals([], ids.collect(true));
  ecs.addEntity({});
  assertEquals([{ id: 0 }], ids.collect(true));
});

Deno.test("queueRemove", async (): Promise<void> => {
  const ecs = createBaseECS();
  const ids = ecs.getComponentIterator(["id"]);
  assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect(true));
  ecs.addToRemoveQueue(1);
  ecs.removeQueued();
  assertEquals([{ id: 0 }, { id: 2 }], ids.collect(true));
});

Deno.test("directRemove", async (): Promise<void> => {
  const ecs = createBaseECS();
  const ids = ecs.getComponentIterator(["id"]);
  assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect());
  ecs.addEntity({});
  ecs.remove(2);
  assertEquals([{ id: 0 }, { id: 1 }, { id: 3 }], ids.collect());
});

Deno.test("addComponent", async (): Promise<void> => {
  const ecs = createBaseECS();
  const ids = ecs.getComponentIterator(["id", "test"]);
  assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect());
  for (const entity of ids) {
    entity.test = 0;
  }
  assertEquals(
    [
      { id: 0, test: 0 },
      { id: 1, test: 0 },
      { id: 2, test: 0 },
    ],
    ids.collect(true)
  );
});

Deno.test("addTypeComponent", async (): Promise<void> => {
  const ecs = new ECS();
  ecs.addTypeEntity("example", { a: 12 });
  const secondEntity = ecs.addTypeEntity("example", { a: 2 });
  const ids = ecs.getTypeIterator("example");
  assertEquals(
    [
      { id: 0, a: 12 },
      { id: 1, a: 2 },
    ],
    ids.collect()
  );
  for (const entity of ids) {
    if (entity.id == secondEntity) {
      entity.b = entity.a;
    }
  }
  assertEquals(
    [
      { id: 0, a: 12 },
      { id: 1, a: 2, b: 2 },
    ],
    ids.collect()
  );
});

Deno.test("setID", async (): Promise<void> => {
  const ecs = new ECS();
  ecs.addTypeEntity("example", { a: 12 });
  const ids = ecs.getTypeIterator("example");
  assertEquals([{ id: 0, a: 12 }], ids.collect(true));
  for (const entity of ids) {
    entity.id = 1;
  }
  assertEquals([{ id: 0, a: 12 }], ids.collect(true));
});

Deno.test("emptyType", async (): Promise<void> => {
  const ecs = new ECS();
  const ids = ecs.getTypeIterator("example");
  assertEquals([], ids.collect());
});

Deno.test("addMidIter", async (): Promise<void> => {
  const ecs = createBaseECS();
  const ids = ecs.getComponentIterator(["id"]);
  assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect(true));
  for (const entity of ids) {
    if (entity.id == 0) {
      ecs.addEntity({});
      ecs.remove(0);
    }
  }
  assertEquals([{ id: 1 }, { id: 2 }, { id: 3 }], ids.collect(true));
});

Deno.test("addSystem", async (): Promise<void> => {
  const ecs = createBaseECS();
  ecs.dispatchSystem([["id"]], (ids: any) => {
    assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect(true));
    ecs.remove(0);
  });
});

Deno.test("addAsyncSystem", async (): Promise<void> => {
  const ecs = createBaseECS();
  await ecs.dispatchSystem([["id"]], async (ids: any) => {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        const collected = ids.collect();
        assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect(true));
        collected[0].health = 69;
        resolve();
      }, 10);
    });
  });
  assertEquals(
    [{ id: 0, health: 69 }, { id: 1 }, { id: 2 }],
    ecs.getComponentIterator(["id", "health"]).collect(true)
  );
});

Deno.test("getType", async (): Promise<void> => {
  const ecs = createBaseECS();
  await ecs.dispatchSystem(["player"], async (ids: any) => {
    assertEquals(0, ids.collect().length);
  });
  ecs.addTypeEntity("player", { uuid: "test" });
  await ecs.dispatchSystem(["player"], async (ids: any) => {
    assertEquals(1, ids.collect().length);
  });
});

Deno.test("extra args", async (): Promise<void> => {
  const ecs = createBaseECS();
  await ecs.dispatchSystem(
    ["player"],
    async (ids: any, extra: number) => {
      assertEquals(10, extra);
    },
    10
  );
  await ecs.dispatchSystem(
    [],
    async (extra: number) => {
      assertEquals(10, extra);
    },
    10
  );
});
