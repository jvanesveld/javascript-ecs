import { assertEquals } from "../deps.ts";
import { ECS } from "../mod.ts";

function createBaseECS() {
  const ecs = new ECS();
  ecs.addEntity({ health: 1 });
  ecs.addEntity({ health: 10 });
  ecs.addEntity({ health: 100 });
  return ecs;
}

Deno.test("get by id", async (): Promise<void> => {
  const source = createBaseECS();
  const testID = source.addTypeEntity("test", { health: 69 });
  const entity = source.getEntity(2);
  assertEquals({ id: 2, health: 100 }, entity);
  entity.health = 10;
  const entity1 = source.getEntity(2);
  assertEquals({ id: 2, health: 10 }, entity1);
  const entities = source.getComponentIterator(["id", "health"]).collect(true);
  assertEquals({ id: 2, health: 10 }, entities[2]);
  assertEquals({ id: testID, health: 69 }, source.getEntity(testID));
});

Deno.test("iter collect setter", async (): Promise<void> => {
  const source = createBaseECS();
  const entities1 = source.getComponentIterator(["id", "health"]).collect();
  entities1[0].health = 10;
  const entities2 = source.getComponentIterator(["id", "health"]).collect(true);
  entities2[0].health = 69;
  const entities3 = source.getComponentIterator(["id", "health"]).collect(true);
  assertEquals(
    [
      { id: 0, health: 10 },
      { id: 1, health: 10 },
      { id: 2, health: 100 },
    ],
    entities3
  );
});
