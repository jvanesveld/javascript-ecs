import { assertEquals } from "../deps.ts";
import { ECS } from "../mod.ts";

function createBaseECS() {
  const ecs = new ECS();
  ecs.addEntity({});
  ecs.addEntity({});
  ecs.addEntity({});
  return ecs;
}

Deno.test("reload", async (): Promise<void> => {
  const source = createBaseECS();
  const ids = source.getComponentIterator(["id"]);
  assertEquals([{ id: 0 }, { id: 1 }, { id: 2 }], ids.collect(true));
  source.remove(0);
  const data = JSON.parse(JSON.stringify(source));
  const target = new ECS(data);
  target.addEntity({});
  const target_ids = target.getComponentIterator(["id"]);
  assertEquals([{ id: 1 }, { id: 2 }, { id: 3 }], target_ids.collect(true));
});
