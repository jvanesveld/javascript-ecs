import { assignIterFunc } from "./iter_functions.ts";

export type ComponentList<T extends Record<string, any>> = {
  id: number[];
  remove: (id: number) => void;
} & {
  [K in keyof T]: T[K][];
} & Iterable<T & { id: number }>;

export type Selector = string | string[];

type EntityData = {
  id: number[];
  [key: string]: any;
};

const reserved = ["id"];

export class ECS {
  #last_id = -1;
  #types: Record<string, EntityData> = {};
  #components: EntityData = {
    id: [],
  };
  #removeQueue: { type?: string; index: number }[] = [];

  constructor(data?: ReturnType<ECS["toJSON"]>) {
    if (data) {
      this.#last_id = data.last_id;
      this.#components = data.components;
      this.#types = data.types;
    }
  }

  async dispatchSystem(selectors: Selector[], handler: Function, ...args: any) {
    const entities = [];
    for (const selector of selectors) {
      if (typeof selector == "string") {
        entities.push(this.getTypeIterator(selector));
      } else {
        entities.push(this.getComponentIterator(selector));
      }
    }
    await handler(...entities, ...args);
  }

  #findID(data: EntityData, id: number) {
    for (let i = 0; i < data.id.length; i++) {
      if (data.id[i] === id) {
        return this.#generateResult(data, i, Object.keys(data));
      }
    }
    return undefined;
  }

  getEntity(id: number) {
    for (const typeData of Object.values(this.#types)) {
      const entity = this.#findID(typeData, id);
      if (entity) {
        return entity;
      }
    }
    return this.#findID(this.#components, id);
  }

  addEntity(components: Record<string, any>) {
    return this.#addEntity(this.#components, components);
  }

  addTypeEntity(type: string, components: Record<string, any>) {
    if (!(type in this.#types)) {
      this.#addType(type);
    }
    return this.#addEntity(this.#types[type], components);
  }

  remove(id: number, type?: string) {
    let index = type
      ? this.#types[type].id.indexOf(id)
      : this.#components.id.indexOf(id);

    if (index === -1) {
      for (const [name, components] of Object.entries(this.#types)) {
        type = name;
        index = components.id.indexOf(id);
        if (index !== -1) {
          break;
        }
      }
    }
    this.#removeEntity(index, type);
  }

  addToRemoveQueue(id: number, type?: string) {
    let index = -1;
    if (type) {
      index = this.#types[type].id.indexOf(id);
    } else {
      index = this.#components.id.indexOf(id);
      if (index === -1) {
        for (const [name, components] of Object.entries(this.#types)) {
          type = name;
          index = components.id.indexOf(id);
          if (index !== -1) {
            break;
          }
        }
      }
    }
    const removed = this.#removeQueue.some(
      (some) => some.type === type && some.index === index
    );
    if (!removed) {
      this.#removeQueue.push({ index, type });
    }
  }

  removeQueued() {
    for (const { index, type } of this.#removeQueue) {
      this.#removeEntity(index, type);
    }
  }

  getTypeIterator(type: string) {
    let self = this;
    return assignIterFunc({
      [Symbol.iterator]: function* () {
        yield* self.#componentIterator(self.#types[type]);
      },
    });
  }

  getComponentIterator(components: string[]) {
    let self = this;
    return assignIterFunc({
      [Symbol.iterator]: function* () {
        yield* self.#componentIterator(self.#components, components);
        for (const typeComponents of Object.values(self.#types)) {
          yield* self.#componentIterator(typeComponents, components);
        }
      },
    });
  }

  toJSON() {
    return {
      last_id: this.#last_id,
      components: this.#components,
      types: this.#types,
    };
  }

  #removeEntity(index: number, type?: string) {
    let components = type ? this.#types[type] : this.#components;
    Object.values(components).forEach((component) =>
      component.splice(index, 1)
    );
  }

  #generateID() {
    this.#last_id += 1;
    return this.#last_id;
  }

  #createProxyHandler(source: EntityData, index: number) {
    return {
      get(obj: any, prop: string) {
        return prop in obj ? obj[prop] : undefined;
      },
      set: (obj: any, prop: string, value: any) => {
        if (prop === "id") {
          return true;
        }
        if (prop in obj) {
          obj[prop] = value;
        } else {
          this.#addComponent(source, prop);
          this.#addValue(obj, source, prop, index);
          obj[prop] = value;
        }
        return true;
      },
    };
  }

  #generateResult(data: EntityData, i: number, components: string[]) {
    const result = {};
    this.#addValue(result, data, "id", i);
    const addComp = components;
    for (const comp of addComp) {
      if (comp !== "id" && data[comp]?.[i] !== undefined) {
        this.#addValue(result, data, comp, i);
      }
    }
    return new Proxy(result, this.#createProxyHandler(data, i));
  }

  *#componentIterator(source: EntityData, components?: string[]) {
    if (!source) {
      return;
    }
    const sourceComponents = Object.keys(source);
    if (
      components &&
      !sourceComponents.some((comp) => components.includes(comp))
    ) {
      return;
    }
    for (let i = 0; i < source.id.length; i++) {
      yield this.#generateResult(source, i, components || sourceComponents);
    }
  }

  #addValue(
    compObject: any,
    source: EntityData,
    component: string,
    index: number
  ) {
    Object.defineProperty(compObject, component, {
      enumerable: true,
      get: function () {
        return source[component][index];
      },
      set: function (value) {
        source[component][index] = value;
      },
    });
  }

  #addType(name: string) {
    this.#types[name] = { id: [] };
  }

  #addComponent(source: EntityData, component: string) {
    const length = source.id.length;
    source[component] =
      source[component] || Array.apply(null, Array(length)).map(() => {});
  }

  #addEntity(source: EntityData, values: Record<string, any>) {
    Object.keys(values).forEach((name) => {
      this.#addComponent(source, name);
    });

    const compArrays = Object.entries(source);
    const id = this.#generateID();
    source.id.push(id);
    for (const [name, array] of compArrays) {
      if (name !== "id") {
        array.push(values[name]);
      }
    }
    return id;
  }
}
