export function assignIterFunc<T>(iterable: Iterable<T>) {
  return Object.assign(iterable, {
    map: <J>(transform: TransformCb<T, J>) => {
      const mapped = reuseGenerator(mapGen, iterable, transform);
      return assignIterFunc(mapped);
    },
    filter: (filter: FilterCb<T>) => {
      const filtered = reuseGenerator(filterGen, iterable, filter);
      return assignIterFunc(filtered);
    },
    collect: (copy = false) => {
      const result = Array.from(iterable);
      if (copy) {
        return Array.from(iterable).map((entity) =>
          typeof entity === "object" ? Object.assign({}, entity) : entity
        );
      }
      return result;
    },
  });
}

type TransformCb<T, J> = (value: T) => J;
type FilterCb<T> = (value: T) => boolean;

function reuseGenerator(gen: any, ...args: any[]) {
  return {
    [Symbol.iterator]: function* () {
      yield* gen(...args);
    },
  };
}

function* mapGen<T, J>(iterable: Iterable<T>, transform: TransformCb<T, J>) {
  for (const value of iterable) {
    yield transform(value);
  }
}

function* filterGen<T>(iterable: Iterable<T>, filter: FilterCb<T>) {
  for (const value of iterable) {
    if (filter(value)) {
      yield value;
    }
  }
}
